class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new("You"), board = Board.new)
    @player = player
    @board = board
  end

  def count
    board.count
  end

  def play_turn
    coordinates = player.get_play
    ship_status = board[coordinates]
    attack(coordinates)
    board.display
    if ship_status == :s
      puts "congrats, it's a hit!"
      puts "there are #{count} ships remaining."
    else
      puts "apologies, you missed..."
      puts "there are #{count} ships remaining."
    end
    puts board.display
  end

  def attack(coordinates)
    board[coordinates] = :x
  end

  def game_over?
    board.won?
  end

  def play
    until game_over?
      play_turn
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  t = BattleshipGame.new
  t.play
end
