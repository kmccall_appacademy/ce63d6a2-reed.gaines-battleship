class Board

  attr_accessor :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    [
    [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
    [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
    [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
    [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
    [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
    [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
    [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
    [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
    [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
    [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil],
    ]
  end

  def display
    puts grid
  end

  def count
    count = 0
    grid.each do |arr|
      arr.each do |pos|
        if pos == :s
          count += 1
        end
      end
    end
    count
  end

  def empty?(arr = nil)
    counter = 0
    grid.each do |a|
      a.each do |pos|
        if pos == nil
          next
        else
          counter += 1
        end
      end
    end
    if counter == 0
      return true
    end

    if arr == nil
      return false
    end

    x = arr[0]
    y = arr[1]
    return true if grid[x][y] == nil
    false
  end

  def full?
    grid.each do |a|
      a.each do |pos|
        if pos == nil
          return false
        end
      end
    end
    true
  end

  def place_random_ship
    num_pool = (0..(grid.length - 1)).to_a
    if full? == false
      grid[num_pool.shuffle[0]][num_pool.shuffle[0]] = :s
    else
      raise 'board is full'
    end
  end

  def self.populate_grid
    while count < 10
      place_random_ship
    end
  end

  def won?
    grid.each do |a|
      a.each do |pos|
        if pos == :s
          return false
        end
      end
    end
    true
  end

  def [](pos)
    grid[pos[0]][pos[1]]
  end

  def []=(pos, val)
    x, y = pos
    grid[x][y] = val
  end

end
