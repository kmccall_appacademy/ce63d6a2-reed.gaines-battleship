class HumanPlayer

  def initialize(name)
    @name = name
  end

  def get_play
    puts "where would you like to bomb? (x, y format)"
    $stdout.flush
    move = gets.chomp.split(", ")
    move.map { |el| el.to_i }
  end

end
